﻿using Microsoft.AspNetCore.Mvc;
using mvcAssignment3.Repository;
using System.Collections.Generic;
using mvcAssignment3.Models;

namespace mvcAssignment3.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeRepository repository;
        public EmployeeController()
        {
            repository = new EmployeeRepository();
        }
        public IActionResult Index()
        {
            List<Employee> emp = repository.GetEmployees();
            return View(emp);
        }

        public IActionResult Details(int Id)
        {
            Employee emp = repository.GetEmployeeById(Id);
            return View(emp);
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Save(Employee emp)
        {
            repository.AddEmployee(emp);
            return RedirectToAction("Index");
        }

        public IActionResult Update(int Id)
        {
            Employee emp = repository.GetEmployeeById(Id);
            return View(emp);
        }

        public IActionResult Edit(int Id)
        {
            repository.EditEmployee(Id);
            return RedirectToAction("Index");
        }
    }
}
