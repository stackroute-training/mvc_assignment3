﻿namespace mvcAssignment3.Models
{
    public class Employee
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public int Salary { get; set; }
        public string Experience { get; set; }
        public string Location { get; set; }

    }
}
