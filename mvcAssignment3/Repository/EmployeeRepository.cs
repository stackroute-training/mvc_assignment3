﻿using mvcAssignment3.Models;
using System.Collections.Generic;
using System.Linq;

namespace mvcAssignment3.Repository
{
    public class EmployeeRepository
    {
        static List<Employee> employees = new()
        {
            new Employee() { Id = 1, Name ="Rico",Email ="rico@emp.com",Designation="trainee",Location="Delhi",Salary=10000},
            new Employee() { Id = 2, Name = "poco", Email = "poco@emp.com", Designation = "sde-1", Location = "Delhi", Salary = 10000 },
            new Employee() { Id = 3, Name = "darryl", Email = "darryl@emp.com", Designation = "sde-2", Location = "Delhi", Salary = 10000 },
            new Employee() { Id = 4, Name = "Rosa", Email = "Rosa@emp.com", Designation = "sde-1", Location = "Delhi", Salary = 10000 }
        };

        public List<Employee> GetEmployees()
        {
            return employees;
        }

        public Employee GetEmployeeById(int id)
        {
            Employee emp = employees.Single(s=>s.Id == id);
            return emp;
        }

        public void AddEmployee(Employee emp)
        {
            employees.Add(emp);
        }

        public Employee EditEmployee(Employee employee)
        {
            Employee emp = GetEmployeeById(employee.Id);

            return employee;
        }
    }
}
